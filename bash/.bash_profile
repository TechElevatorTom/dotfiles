export JAVA_HOME=$(/usr/libexec/java_home)
export PATH=${JAVA_HOME}/bin:$PATH
export PATH=~/dev-tools/apache-maven-3.3.9/bin:$PATH
export PATH=~/dev-tools/apache-tomcat-9.0.0.M17/bin:$PATH
#export PATH=/Applications/Postgres.app/Contents/Versions/latest/bin:$PATH
export PATH=/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin:$PATH
#export PATH=/Library/PostgreSQL/10/bin:$PATH
#export PATH=/Users/tommedvitz/PostgreSQL/pg10/bin:$PATH

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
source ~/git-completion.bash
#alias pip=pip3
#alias python='/usr/local/bin/python3'
